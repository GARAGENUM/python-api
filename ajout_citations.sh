#!/bin/bash

# ADMIN KEY
API_KEY="A RECUPERER DES LOGS DOCKER"

########################## LIST CITATIONS ##########################

API_ENDPOINT="http://localhost:8000/api/citations/add_list"
CITATIONS='{
  "citations": [
    {"auteur": "Maya Angelo", "citation": "La vie n´est pas mesurée par le nombre de respirations que nous prenons, mais par les moments qui nous coupent le souffle."},
    {"auteur": "Leonardo da Vinci", "citation": "La simplicité est la sophistication ultime."},
    {"auteur": "Albert Einstein", "citation": "La folie, c´est de faire la même chose et de s´attendre à un résultat différent."},
    {"auteur": "Mark Twain", "citation": "La gentillesse est la langue que les sourds peuvent entendre et les aveugles peuvent voir."},
    {"auteur": "Henry Ford", "citation": "L´échec est simplement l´opportunité de recommencer, cette fois d´une manière plus intelligente."},
    {"auteur": "Oscar Wilde", "citation": "La vie est trop importante pour être prise au sérieux."},
    {"auteur": "Neale Donald Walsch", "citation": "La vie commence à la fin de votre zone de confort."},
    {"auteur": "Mahatma Gandhi", "citation": "Soyez le changement que vous voulez voir dans le monde."},
    {"auteur": "Muhammad Ali", "citation": "Ne comptez pas les jours, faites que les jours comptent."},
    {"auteur": "Sénèque", "citation": "La vie n´est pas d´attendre que l´orage passe, mais d´apprendre à danser sous la pluie."},
    {"auteur": "Steve Jobs", "citation": "Votre temps est limité, ne le gaspillez pas à vivre la vie de quelqu´un d´autre."},
    {"auteur": "Ralph Marston", "citation": "Le bonheur est un choix, pas une résultante. Rien ne vous rend heureux à moins que vous ne décidiez de l´être."},
    {"auteur": "Vidal Sassoon", "citation": "Le seul endroit où le succès vient avant le travail, c´est dans le dictionnaire."},
    {"auteur": "Dalaï Lama", "citation": "Il n´y a personne qui soit né sous une mauvaise étoile, il n´y a que des gens qui ne savent pas lire le ciel."}
  ]
}'

# AJOUT CITATION
curl -X POST -H "Content-Type: application/json; charset=utf-8" -H "API-Key: $API_KEY" -d "$CITATIONS" "$API_ENDPOINT"

# VERIF ALL CITATIONS
curl -H "API-Key: $API_KEY" http://localhost:8000/api/citations/all | jq