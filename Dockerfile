FROM python:3.11-slim-bookworm

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app
COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt
COPY app/. .

CMD ["gunicorn", "-w", "4", "wsgi:app", "--bind", "0.0.0.0:8000"]