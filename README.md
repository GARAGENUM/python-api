# PYTHON API WITH FLASK

Base for create an API for french quotes in flask with SQL Alchemy for backend

## CONFIGURATION :wrench:

- In app/app.py configure admin_api_key and user_api_key

## UTILISATION :checkered_flag:

- créer un evironnement virtuel:
```bash
sudo apt install python3-venv
python3 -m venv venv
```

- Installation des dépendances
```bash
pip install -r requirements.txt
cd app
flask run
```

> Récupérer l'api_key admin qui s'affiche dans le terminal

- Ajouter les deux citations test:
```bash
curl -X POST -H "Content-Type: application/json; charset=utf-8" -H "API-Key: <admin_api_key>" -d '{"auteur": "François de La Rochefoucauld", "citation": "Il est plus aisé d’etre sage pour les autres que de l’etre pour soi-même."}' http://localhost:5000/api/citations/add

curl -X POST -H "Content-Type: application/json; charset=utf-8" -H "API-Key: <admin_api_key>" -d '{"auteur": "Pierre De Montesquieu", "citation": "On ne justifie pas une injustice par une autre."}' http://localhost:5000/api/citations/add
```

- Ajouter une liste de citations:
```bash
curl -X POST -H "Content-Type: application/json; charset=utf-8" -H "API-Key: <admin_api_key>" -d '{
  "citations": [
    {"auteur": "Auteur1", "citation": "Citation1"},
    {"auteur": "Auteur2", "citation": "Citation2"},
    {"auteur": "Auteur3", "citation": "Citation3"}
  ]
}' http://localhost:5000/api/citations/add_list
```

- API admin:
```bash
curl -H "API-Key: <admin_api_key>" http://localhost:5000/api/citations/all | jq
curl -H "API-Key: <admin_api_key>" http://localhost:5000/api/citations/random | jq
```


- Ajouter un utilisateur non admin:
```bash
curl -X POST -H "Content-Type: application/json; charset=utf-8" -H "API-Key: <admin_api_key>" -d '{"username": "nouvel_utilisateur", "is_admin": false}' http://localhost:5000/api/users/add
```

- Supprimer un utilisateur:
```bash
curl -X DELETE -H "Content-Type: application/json; charset=utf-8" -H "Api-Key: <admin_api_key>" -d '{"username": "nom_utilisateur_a_supprimer"}' http://localhost:5000/api/users/delete

```

> Récupérer l'api_key qui s'affiche pour le nouvel utilisateur

- API user:
```bash
curl -H "API-Key: <nouvel_utilisateur_api_key>" http://localhost:5000/api/citations/random | jq
```

:bulb: Utiliser le script ajout_citations.sh pour ajouter plusieurs citations en une fois

## DOCKER :whale:

- Construire l'image Docker et démarrer un conteneur:
```bash
docker build -t api:1.0 .
docker run -d -p 8000:8000 api:1.0
```

### DOCKER COMPOSE

- Démarrer l'API:
```bash
docker-compose up -d
```

## CLIENT ANDROID :iphone:

[Exemple de client Android](https://git.legaragenumerique.fr/GARAGENUM/python-kivy)

## TO DO :bookmark_tabs:

- [X] fonction check si citation existe deja
- [X] fonction add user
- [X] test added user
- [X] fonction ajout_citations(list)
- [X] tester Docker
- [X] gérer l'UTF8
- [X] endpoint del user


